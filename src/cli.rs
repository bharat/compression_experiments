use clap::Parser;
use lsharp_ru::{
    learner::l_sharp::{Rule2, Rule3},
    oracles::equivalence::InfixStyle,
};

#[derive(Parser, derive_getters::Getters)]
#[command(author, version, about)]
pub struct Cli {
    #[arg(long, value_enum, default_value_t)]
    rule2: Rule2,
    #[arg(long, value_enum, default_value_t)]
    rule3: Rule3,
    #[arg(short = 'm', long)]
    model: String,
    #[arg(short = 'x', long, default_value_t = 42)]
    seed: u64,
    #[arg(long)]
    eq_mode: InfixStyle,
    #[arg(short = 'k', long)]
    extra_states: usize,
    #[arg(short = 'l', long)]
    expected_random_length: usize,
    #[arg(short = 'o', long)]
    out: String,
    #[arg(short = 'v')]
    /// Set verbosity level for logs.
    ///
    /// 0 => disabled, 1 => Info, 2 => Debug, 3 => Trace.
    verbosity: usize,
    #[arg(short = 'q', default_value_t)]
    /// Quiet mode (false by default).
    /// Do not print anything to stdio.
    quiet: bool,
    #[arg(long, default_value_t)]
    /// Compress the tree. Currently broken, do not use.
    compress_tree: bool,
}
