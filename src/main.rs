mod cli;

use clap::Parser;
use log::LevelFilter;
use log4rs::append::file::FileAppender;
use log4rs::config::{Appender, Root};
use log4rs::encode::pattern::PatternEncoder;
use log4rs::Config;
use lsharp_ru::definitions::mealy::{InputSymbol, Mealy, OutputSymbol};
use lsharp_ru::learner::obs_tree::compressed::CompObsTree;
use lsharp_ru::learner::obs_tree::normal::MapObsTree;
use lsharp_ru::sul::{Simulator, SystemUnderLearning};
use lsharp_ru::util::parsers::machine::read_mealy_from_file;
use std::collections::HashMap;
use std::fmt::Debug;
use std::fs;
use std::hash::BuildHasherDefault;
use std::path::Path;

type DefaultFxHasher = BuildHasherDefault<rustc_hash::FxHasher>;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    println!("Hello, world!");
    // Create hypothesis and log directories
    ensure_dir_exists("./hypothesis")?;
    // Create the log directory manually before execution.
    // ensure_dir_exists("./log")?;
    let logfile = FileAppender::builder()
        .encoder(Box::new(PatternEncoder::new(
            "{d(%Y-%m-%d %H:%M:%S)} {l} {M} | {m}{n}",
        )))
        .build(format!("log/{}.log", chrono::Utc::now()))?;

    let args = cli::Cli::parse();

    let rule2_mode = args.rule2();
    let rule3_mode = args.rule3();

    let path_name = args.model();
    let use_ly_ads = false;
    let extra_states = *args.extra_states();

    let rnd_infix_len = *args.expected_random_length();

    let seed = *args.seed();

    let infix_style = *args.eq_mode();

    let logging_level = *args.verbosity();
    let logging_level = match logging_level {
        0 => LevelFilter::Off,
        1 => LevelFilter::Info,
        2 => LevelFilter::Debug,
        _ => LevelFilter::Trace,
    };

    let file = args.out();
    let is_quiet = *args.quiet();
    let compress_tree = *args.compress_tree();
    let config = Config::builder()
        .appender(Appender::builder().build("logfile", Box::new(logfile)))
        .build(Root::builder().appender("logfile").build(logging_level))?;
    log4rs::init_config(config)?;
    log::info!("ClosingStrategy : ADS");
    log::info!("ObservationTableCEXHandler: N/A");
    log::info!("Method : L# ({:?}, {:?})", rule2_mode, rule3_mode);
    log::info!("Seed : {seed}");
    log::info!("EquivalenceOracle settings: (H-ADS, {extra_states}, {rnd_infix_len})");
    log::info!("Extra States : {extra_states}");
    log::info!("Random Infix Length : {rnd_infix_len}");
    log::info!("Cache : Y");
    let (input_map, output_map, mealy_machine) = read_fsm(path_name);

    let sim_sul = Simulator::new(&mealy_machine, &input_map, &output_map);
    let comp_tree = CompObsTree::<DefaultFxHasher>::new(sim_sul.input_map().len());
    let _naive_tree = MapObsTree::<DefaultFxHasher>::new(sim_sul.input_map().len());
    Ok(())
}

fn read_fsm(path_name: &String) -> (HashMap<String, InputSymbol>, HashMap<String, OutputSymbol>, Mealy) {
    let file_name = Path::new(path_name);
    let (fsm, input_map, output_map) = prep_fsm_from_file(file_name.to_str().expect("Safe"))
        .unwrap_or_else(|_| {
            panic!(
                "Error reading from DOT file for {}",
                file_name.to_str().expect("Safe"),
            )
        });
    let mealy_machine = fsm;
    (input_map, output_map, mealy_machine)
}

fn ensure_dir_exists(dir_name: &str) -> std::io::Result<()> {
    // Do not remove any of these lines
    // Remove the dir (returns an error if it doesn't exist)
    // and ignore the error-case.
    let dir = Path::new(dir_name);
    // let dir = dir.canonicalize()?;
    if dir.exists() {
        fs::remove_dir_all(dir_name)?;
    }
    // Create an empty dir.
    fs::create_dir_all(dir)
}

type ParseResultFSM = Result<
    (
        Mealy,
        HashMap<String, InputSymbol>,
        HashMap<String, OutputSymbol>,
    ),
    Box<dyn std::error::Error>,
>;

fn prep_fsm_from_file<P>(file_name: P) -> ParseResultFSM
where
    P: AsRef<Path> + Debug,
{
    ensure_dir_exists("./hypothesis")?;
    log::info!("Read file from {:?}", file_name);
    Ok(read_mealy_from_file(file_name))
}
